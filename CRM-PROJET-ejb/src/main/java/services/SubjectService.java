package services;

import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import Interfaces.subjectServiceRemote;
import model.Subject;
@Stateless
@LocalBean
public class SubjectService implements subjectServiceRemote {
	@PersistenceContext
	EntityManager em;

	@Override
	public int ajouterSubject(Subject subject) {
		Date releaseDate=new Date();
		subject.setReleaseDate(releaseDate);
		em.persist(subject);
		return subject.getIdSubject();
	}
	@Override
	public void deleteSubjectById(int idSubject) {
		Subject sub= em.find(Subject.class, idSubject);
		
		//Desaffecter l'employe de tous les departements
		//c'est le bout master qui permet de mettre a jour
		//la table d'association
		em.remove(sub);		
	}

	@Override
	public void updateSubject(Subject subject) {
		em.merge(subject); 		
	}

	@Override
	public List<Subject> getAllSubjects() {
		List<Subject> sub = em.createQuery("Select e from Subject e", Subject.class).getResultList();
		System.out.println(sub);
		return sub;
	}

}
