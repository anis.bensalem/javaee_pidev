package services;

import java.util.Date;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import Interfaces.CommentServiceRemote;
import model.Comment;
import model.Subject;

@Stateless
@LocalBean
public class CommentService implements CommentServiceRemote {
	

	public CommentService() {
		// TODO Auto-generated constructor stub
	}
	@PersistenceContext
	EntityManager em;
	
	@Override
	public void deleteCommentById(int idResponse) {
		em.remove(em.find(Comment.class,idResponse ));
	}

	@Override
	public int ajouterComment(Comment comment) {
		//Date releaseDate=new Date();
		//comment.setReleaseDate(releaseDate);
		em.persist(comment);
		return comment.getIdResponse();
	}
	@Override
	public List<Comment> getAllResponse() {
		List<Comment> comm = em.createQuery("Select c from Comment c", Comment.class).getResultList();
		return comm;
	}

	@Override
	public void updateComment(Comment comment) {
		Comment comm = em.find(Comment.class,comment.getIdResponse()); 
		comm.setResponseBody(comment.getResponseBody()); 
			
	}
	

}
