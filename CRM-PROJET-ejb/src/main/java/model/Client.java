package model;

import java.io.Serializable;
import javax.persistence.*;
import java.sql.Date;
import java.util.List;


/**
 * The persistent class for the Clients database table.
 * 
 */
@Entity
@Table(name="Clients")
@NamedQuery(name="Client.findAll", query="SELECT c FROM Client c")
public class Client implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name="ClientId")
	private int clientId;

	@Column(name="CIN")
	private int cin;

	@Column(name="City")
	private String city;

	@Column(name="DateOfBirth")
	private Date dateOfBirth;

	@Column(name="NameCl")
	private String nameCl;

	@Column(name="Phone")
	private int phone;

	@Column(name="PhotoCl")
	private String photoCl;

	//bi-directional many-to-one association to Subject
	@OneToMany(mappedBy="client")
	private List<Subject> subjects;

	public Client() {
	}

	public int getClientId() {
		return this.clientId;
	}

	public void setClientId(int clientId) {
		this.clientId = clientId;
	}

	public int getCin() {
		return this.cin;
	}

	public void setCin(int cin) {
		this.cin = cin;
	}

	public String getCity() {
		return this.city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getDateOfBirth() {
		return this.dateOfBirth;
	}

	public void setDateOfBirth(Date dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public String getNameCl() {
		return this.nameCl;
	}

	public void setNameCl(String nameCl) {
		this.nameCl = nameCl;
	}

	public int getPhone() {
		return this.phone;
	}

	public void setPhone(int phone) {
		this.phone = phone;
	}

	public String getPhotoCl() {
		return this.photoCl;
	}

	public void setPhotoCl(String photoCl) {
		this.photoCl = photoCl;
	}

	public List<Subject> getSubjects() {
		return this.subjects;
	}

	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}

	public Subject addSubject(Subject subject) {
		getSubjects().add(subject);
		subject.setClient(this);

		return subject;
	}

	public Subject removeSubject(Subject subject) {
		getSubjects().remove(subject);
		subject.setClient(null);

		return subject;
	}

}