package model;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

import javax.persistence.*;


/**
 * The persistent class for the Subjects database table.
 * 
 */
@Entity
@Table(name="Subjects")
@NamedQuery(name="Subject.findAll", query="SELECT s FROM Subject s")
public class Subject implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int idSubject;
	private Date releaseDate=new Date();
	private String subjectBody;
	private String title;

	//bi-directional many-to-one association to Client
	@ManyToOne
	@JoinColumn(name="ClientId",nullable=true)
	private Client client;

	public Subject() {
	}
	
	public int getIdSubject() {
		return this.idSubject;
	}

	public void setIdSubject(int idSubject) {
		this.idSubject = idSubject;
	}

	public String getSubjectBody() {
		return this.subjectBody;
	}

	public void setSubjectBody(String subjectBody) {
		this.subjectBody = subjectBody;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Client getClient() {
		return this.client;
	}

	public void setClient(Client client) {
		this.client = client;
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	public Date getReleaseDate() {
		return releaseDate;
	}

	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}

	public Subject(String title,String subjectBody) {
		super();
		this.subjectBody = subjectBody;
		this.title = title;
	}

	public Subject(String subjectBody, String title,int idSubject) {
		super();
		this.idSubject = idSubject;
		this.subjectBody = subjectBody;
		this.title = title;
	}
	

	

	


}