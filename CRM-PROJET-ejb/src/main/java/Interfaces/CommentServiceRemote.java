package Interfaces;

import java.util.List;

import javax.ejb.Remote;

import model.Comment;
import model.Subject;
@Remote
public interface CommentServiceRemote {
	public int ajouterComment(Comment comment);
	public void deleteCommentById(int idResponse);
	public List<Comment> getAllResponse();
	public void updateComment(Comment comment);
}
