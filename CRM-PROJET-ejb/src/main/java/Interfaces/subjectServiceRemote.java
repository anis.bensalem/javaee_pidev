package Interfaces;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import model.Comment;
import model.Subject;

@Remote
public interface subjectServiceRemote {
	public int ajouterSubject(Subject subject);
	public void deleteSubjectById(int idSubject);
	public void updateSubject(Subject subject); 
	public List<Subject> getAllSubjects();
	
	
	
	


}
