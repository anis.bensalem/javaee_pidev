package ManagedBean;

import java.io.Serializable;
import java.sql.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import model.Comment;
import model.Subject;
import services.CommentService;
import services.SubjectService;
@ManagedBean(name="commentBean")
@SessionScoped
public class CommentBean implements Serializable {
	private int clientId;
	private int idSubject;
	private Date releaseDate;
	private String responseBody;
	private List<Comment> comments;
	private int selectedSubjectId;
	private Subject OneSub;
	@EJB
	CommentService commentService;
	@EJB
	 SubjectService subjectService;
	public CommentBean() {
		// TODO Auto-generated constructor stub
	}
	public List<Comment> getAllComments(){
		comments=commentService.getAllResponse();
		return comments;
	}
	public void addComment() {
		int subjectId=0;
		OneSub=subjectService.getSubjectById(subjectId);
		String navigateTo=null;
		this.setSelectedSubjectId(OneSub.getIdSubject());
		Comment comment=new Comment(clientId,responseBody);
	//	Subject selectedSubject=new Subject();
		//selectedSubject.setIdSubject(selectedSubjectId);
		//comment.setIdSubject(selectedSubjectId);
		System.out.println(selectedSubjectId);
		int id=commentService.ajouterComment(comment);
		subjectService.affecterCommentSubject(selectedSubjectId, id);
		navigateTo="/pages/subject/displaySubjects?faces-redirect=true";
		
	}
	
	
	
	
	
	
	public void deleteComment(int idResponse) {
		commentService.deleteCommentById(idResponse);;
	}
	public void modifierComment() {
		commentService.updateComment(new Comment(responseBody));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	public int getClientId() {
		return clientId;
	}
	public void setClientId(int clientId) {
		this.clientId = clientId;
	}
	public int getIdSubject() {
		return idSubject;
	}
	public void setIdSubject(int idSubject) {
		this.idSubject = idSubject;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public String getResponseBody() {
		return responseBody;
	}
	public void setResponseBody(String responseBody) {
		this.responseBody = responseBody;
	}
	public List<Comment> getComments() {
		return comments;
	}
	public void setComments(List<Comment> comments) {
		this.comments = comments;
	}
	public int getSelectedSubjectId() {
		return selectedSubjectId;
	}
	public void setSelectedSubjectId(int selectedSubjectId) {
		this.selectedSubjectId = selectedSubjectId;
	}
	public CommentService getCommentService() {
		return commentService;
	}
	public void setCommentService(CommentService commentService) {
		this.commentService = commentService;
	}
	public Subject getOneSub() {
		return OneSub;
	}
	public void setOneSub(Subject oneSub) {
		OneSub = oneSub;
	}
	public SubjectService getSubjectService() {
		return subjectService;
	}
	public void setSubjectService(SubjectService subjectService) {
		this.subjectService = subjectService;
	}
	

}
