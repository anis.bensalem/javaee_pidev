package ManagedBean;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UpdateModelException;
import javax.faces.context.FacesContext;

import model.Client;
import model.Subject;
import services.SubjectService;


@ManagedBean(name="subjectBean")
@SessionScoped
public class SubjectBean implements Serializable {
	private String subjectBody;
	private String title;
	private Date releaseDate=new Date();
	private Client client;
	private List<Subject> subjects;
	private int idSubject;
	private int idSubjectToBeUpdated;
	private Subject OneSub;

	public SubjectBean() {
		// TODO Auto-generated constructor stub
	}
	@EJB
    SubjectService subjectService;	
	
	public void addSubject() {
subjectService.ajouterSubject(new Subject(title, subjectBody));
System.out.println(title+"jfdgjdggdgdgdgggggg"+subjectBody);
}
	
	public List<Subject> getSubjects() {
		subjects=subjectService.getAllSubjects();
		return subjects;
	}
	
	public String getOneSubject(int subjectId) {
		System.out.println(subjectId);
		OneSub=subjectService.getSubjectById(subjectId);
		String navigateTo=null;
		System.out.println(OneSub.getIdSubject());
		if(OneSub != null ) {
		navigateTo="/pages/Comment/LeaveReplay?faces-redirect=true";}
		else {
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("Bad "));
		}
		return navigateTo;
	}
	
	public void deleteSubject(int idSubject) {
		subjectService.deleteSubjectById(idSubject);
	}
	public String modifierSubject(Subject subject) {
		String navigateTo=null;
		this.setTitle(subject.getTitle());
		this.setSubjectBody(subject.getSubjectBody());
		this.setIdSubjectToBeUpdated(subject.getIdSubject());
		navigateTo="/pages/subject/updateSubject?faces-redirect=true";
		return navigateTo;}
	
	
	public String mettreajourSubject() {
		String navigateTo=null;
		subjectService.updateSubject(new Subject(title,subjectBody,idSubjectToBeUpdated));
		navigateTo="/pages/subject/displaySubjects?faces-redirect=true";
		return navigateTo; }

	
	public String getSubjectBody() {
		return subjectBody;
	}
	public void setSubjectBody(String subjectBody) {
		this.subjectBody = subjectBody;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public SubjectService getSubjectService() {
		return subjectService;
	}
	public void setSubjectService(SubjectService subjectService) {
		this.subjectService = subjectService;
	}
	
	public void setSubjects(List<Subject> subjects) {
		this.subjects = subjects;
	}
	public Client getClient() {
		return client;
	}
	public void setClient(Client client) {
		this.client = client;
	}
	public Date getReleaseDate() {
		return releaseDate;
	}
	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}
	public int getIdSubject() {
		return idSubject;
	}
	public void setIdSubject(int idSubject) {
		this.idSubject = idSubject;
	}

	public int getIdSubjectToBeUpdated() {
		return idSubjectToBeUpdated;
	}

	public void setIdSubjectToBeUpdated(int idSubjectToBeUpdated) {
		this.idSubjectToBeUpdated = idSubjectToBeUpdated;
	}

	public Subject getOneSub() {
		return OneSub;
	}

	public void setOneSub(Subject oneSub) {
		OneSub = oneSub;
	}
	
	
	
	


}
